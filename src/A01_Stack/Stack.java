package A01_Stack;


public class Stack<T> {
    private Node<T> first;

    /**
     * Oberstes Element entfernen und zurückliefern.
     * Existiert kein Element, wird eine Exception ausgelöst.
     *
     * @throws StackEmptyException
     */
    public T pop() throws StackEmptyException {
        if (first != null){
            Node<T> node = first;
            if (first.getNext() != null)
                first = node.getNext();
            else
                first = null;
            return node.getData();
        /*if (first != null) {
            Node<T> node = first;
            if (first.getNext() != null)
                first = first.getNext();
            else
                first = null;
            return node.getData();*/
        } else
            throw new StackEmptyException();
    }

    /**
     * Übergebenen T auf Stack (als oberstes Element) speichern.
     *
     * @param i data
     */
    public void push(T i) {
        Node<T> node = new Node<>(i);
        if (first != null)
            node.setNext(first);
        else
        first = node;
        /*Node<T> node = new Node<>(i);
        if (first != null)
            node.setNext(first);
        first = node;*/
    }

    /**
     * Liefert die Anzahl der Elemente im Stack
     *
     * @return
     */
    public int getCount() {
        int counter = 0;
        Node<T> node;
        if (first != null)
            node = first;
        else
            return counter;
        while (node != null){
            node = node.getNext();
            counter++;
        }
        return counter;
        /*if (first != null) {
            Node<T> node = first;
            int counter = 0;
            while (node != null) {
                counter++;
                node = node.getNext();
            }
            return counter;
        }
        return 0;*/
    }
}
