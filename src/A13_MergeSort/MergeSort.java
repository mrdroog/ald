package A13_MergeSort;

import java.util.Arrays;


public class MergeSort implements PersonenSort {
	
	/**
	 * Sortier-Funktion
	 */
	public void sort(Person[] personen) {
		// Start des rekursiven Aufrufs
		sort(personen, 0, personen.length-1);
	}

	/**
	 * Rekursive Funktion zum Sortieren eines Teils des Arrays
	 * @param personen Zu sortierendes Array
	 * @param start    Startpunkt im Array
	 * @param end      Endpunkt im Array
	 */
	public void sort(Person[] personen, int start, int end)
	{
		// TODO: Aufteilung & Rekursion implementieren
		if (personen.length < 2)
			return;
		int mitte = end / 2;
		
		// F�r Merge: H�lften in eigene Arrays kopieren
		// Hinweis: bei copyOfRange ist Obergrenze exklusiv, deshalb "+ 1"
		Person[] teil1 = Arrays.copyOfRange(personen, start, mitte+1);
		Person[] teil2 = Arrays.copyOfRange(personen, mitte+1, end+1);
		sort(teil1);
		sort(teil2);
		// Beide H�lften zusammenf�gen und in data-Array schreiben
		merge(teil1, teil2, personen, start);
	}

	/**
	 * Merge zweier Arrays in ein Ergebnis-Array
	 * @param pers1 Erstes Array
	 * @param pers2 Zweites Array
	 * @param result Ergebnisarray
	 * @param start  Position f�r Speicherung in Ergebnisarray
	 */
	public void merge(Person[] pers1, Person[] pers2, Person[] result, int start) {

		// TODO: Merge implementieren
		int i = 0;
		int j = 0;
		int k = start;

		while (i < pers1.length && j < pers2.length) {
			if (pers1[i].compareTo(pers2[j]) <= 0) {
				result[k] = pers1[i];
				i++;
			} else {
				result[k] = pers2[j];
				j++;
			}
			k++;
		}

		while( i < pers1.length ) {
			result[k] = pers1[i];
			i++;
			k++;
		}

		/*while( j < pers2.length ) {
			result[k] = pers1[j];
			j++;
			k++;
		}*/
	}

}
