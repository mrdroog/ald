package A10_DijkstraPQShortestPath;

import java.util.List;

public class DijkstraPQShortestPath extends FindWay {
	private int[] dist;

	public DijkstraPQShortestPath(Graph graph) {
		super(graph);
	}

	/**
	 * Startentfernung initialisieren
	 * 
	 //* @param from
	 *            Startknoten
	 */
	protected void initPathSearch() {
		int numv = graph.numVertices();
		dist = new int[numv];
		for (int i = 0; i < numv; i++) {
			dist[i] = 9999; // Summen im Graph d�rfen nie mehr ergeben
		}
	}

	/**
	 * Berechnet *alle* k�rzesten Wege ausgehend vom Startknoten Setzt dist[]-
	 * und pred[]-Arrays, kein R�ckgabewert
	 * 
	 * @param from
	 *            Startknoten
	 */
	protected boolean calculatePath(int from, int to) {
		// TODO: IHRE IMPLEMENTIERUNG
		VertexHeap vertexHeap = new VertexHeap(graph.numVertices());
		for (int i = 0; i < graph.numVertices(); i++) {
			vertexHeap.insert(new Vertex(i, dist[i]));	//F�r jeden Vertex des Graphen einen Vertex mit Priority im Heap anlegen.
			pred[i] = -1;	//Vorg�nger zur�cksetzen.
		}
		vertexHeap.setCost(from, 0);	//Priority des Startvertex auf "0" setzen.
		dist[from] = 0;	//Distanz vom Startknoten zum Startknoten auf "0".
		while (!vertexHeap.isEmpty()){	//Solange es noch Vertizes im Heap gibt.
			Vertex current = vertexHeap.remove();
			if (current.vertex == to)
				break;
			List<WeightedEdge> edges = graph.getEdges(current.vertex);
			for (WeightedEdge edge : edges) {
				if (dist[edge.to_vertex] > (dist[current.vertex] + edge.weight)){
					dist[edge.to_vertex] = (dist[current.vertex] + edge.weight);
					pred[edge.to_vertex] = current.vertex;
					vertexHeap.setCost(edge.to_vertex, dist[edge.to_vertex]);
				}
			}
		}
		return true;
	}
}
