package A08_GraphZusammenh�ngend;

import basisAlgorithmen.Graph;
import basisAlgorithmen.Vertex;
import basisAlgorithmen.WeightedEdge;

import java.util.*;

public class ConnectedComponents {
	
	/**
	 * Retourniert die Anzahl der zusammenh�ngenden Komponenten eines Graphen
	 * @param g zu pr�fender Graph
	 * @return Anzahl der Komponenten
	 */
	public int getNumberOfComponents(Graph g) {

		Stack<Integer> stack = new Stack<>();
		Set<Integer> visited = new HashSet<>();
		int components = 0;
		stack.push(0);
		while (!stack.empty()){
			List<WeightedEdge> list = g.getEdges(stack.pop());
			for (WeightedEdge edge : list) {
				if (visited.add(edge.to_vertex))
					stack.add(edge.to_vertex);
			}
		}

		return 0;
	}
}
