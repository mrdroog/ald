package A07_BubbleSort;


public class BubbleSort implements PersonenSort {

    /**
     * Sortier-Funktion
     */
    public void sort(Person[] personen) {
        boolean switched = true;
        for (int i = 0; i < personen.length - 1; i++) {
            if (switched == false)
                break;
            else
                switched = false;
            for (int j = 0; j < personen.length - i - 1; j++) {
                if (personen[j].compareTo(personen[j + 1]) > 0) {
                    Person tmp = personen[j + 1];
                    personen[j + 1] = personen[j];
                    personen[j] = tmp;
                    switched = true;
                }
            }
        }
    }
}

