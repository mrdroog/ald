package A12_DijkstraLand;

import java.util.ArrayList;
import java.util.List;

public class Dijkstra {

	public static List<Integer> dijkstra(Graph g, int von, int nach) {
		
		int[] pred = new int[g.numVertices()];
		int[] dist = new int[g.numVertices()];
	
		// TODO
		VertexHeap vertexHeap = new VertexHeap(g.numVertices());
		for (int i = 0; i < g.numVertices(); i++) {
			dist[i] = 9999;
			//vertexHeap.insert();	???
		}


		// pred ausgeben
		for(int i=0; i<pred.length; i++) {
			System.out.println(i + " �ber " + pred[i]);
		}
		
		
		// Way ausgeben
		System.out.println();
		ArrayList<Integer> way = predToWay(pred, von, nach);
		return way;
	}
	
	private static ArrayList<Integer> predToWay(int[] pred, int from, int to) {
		
		ArrayList<Integer> way = new ArrayList<>();
		
		// TODO
		int current = to;
		while (current != from){
			way.add(current);
			current = pred[current];
		}
		way.add(from);
		return way;
	}
	

}
