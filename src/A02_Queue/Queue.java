package A02_Queue;

public class Queue<T>
{
    private Node<T> first;
    
    private Node<T> last;
    /**
     * Das vorderste (=erste) Element aus der Queue entfernen und zurückliefern.
     * Existiert kein Element, wird eine Exception ausgelöst.
     * @throws QueueEmptyException 
     */
    public T dequeue() throws QueueEmptyException {
        if (first != null){
            Node<T> result = first;
            if (first.getNext() != null)
                first = first.getNext();
            else
                first = null;
            return result.getData();
        }else {
            throw new QueueEmptyException();
        }
        /*if (first != null){
            Node<T> node = first;
            if (first.getNext() != null)
                first = first.getNext();
            else
                first = null;
            return node.getData();
        } else
            throw new QueueEmptyException();*/
    }
    
    
    
    /**
     * Übergebenen Integer am Ende der Queue anhängen.
     * @param i Zahl
     */
    public void enqueue(T i) {
        if (first != null){
            Node<T> current = first;
            while (current.getNext() != null)
                current = current.getNext();
            current.setNext(new Node<>(i));
            last = current.getNext();
        } else{
            first = new Node<>(i);
            last = first;
        }
        /*if (first != null){
            Node<T> node = first;
            while (node.getNext() != null)
                node = node.getNext();
            Node<T> last = new Node<>(i);
            node.setNext(last);
        } else
            first = new Node<>(i);*/
    }
    
    /**
     * Liefert die Anzahl der Elemente im Stack
     * @return
     */
    public int getCount() {
        if (first != null){
            Node<T> node = first;
            int counter = 0;
            while (node != null){
                node = node.getNext();
                counter++;
            }
            return counter;
        }
    	return 0;
    }
}
