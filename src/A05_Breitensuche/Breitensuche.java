package A05_Breitensuche;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Breitensuche extends BaseTree<Integer> {

	@Override
	protected int compare(Integer a, Integer b) {
		return a.compareTo(b);
	}

	/**
	 * Liefert Knoten des Baums ausgehend von Start in Reihenfolge der Breitensuche zur�ck
	 * @param start Startknoten f�r Teilbaum
	 * @return Liste der Knoten in Breitenfolge
	 */
	public List<Integer> getBreadthFirstOrder(Node<Integer> start) {
		List<Integer> list = new ArrayList<>();
		Queue<Node> queue = new LinkedList<>();
		queue.add(start);
		while (!queue.isEmpty()){
			Node<Integer> node = queue.remove();
			list.add(node.getValue());
			if (node.getLeft() != null)
				queue.add(node.getLeft());
			if (node.getRight() != null)
				queue.add(node.getRight());
		}
		return list;
	}

	/**
	 * Liefert Knoten des Baums ausgehend von Start in Reihenfolge der Breitensuche zur�ck,
	 * allerdings nur jene Knoten, die in der angegebenen Ebene liegen (Start hat Ebene=1)
	 * @param start Startknoten f�r Teilbaum
	 * @param level Nur Knoten dieser Ebene ausgeben
	 * @return Liste aller Knoten
	 */
	public List<Integer> getBreadthFirstOrderForLevel(Node<Integer> start, int level) {
		List<Integer> result = new ArrayList<>();
		Queue<Node> queue = new LinkedList<>();
		start.setLevel(1);
		queue.add(start);
		while (!queue.isEmpty()){
			Node<Integer> node = queue.remove();
			if (node.getLevel() == level)
				result.add(node.getValue());
			if (node.getLeft() != null){
				node.getLeft().setLevel(node.getLevel() + 1);
				queue.add(node.getLeft());
			}
			if (node.getRight() != null){
				node.getRight().setLevel(node.getLevel() + 1);
				queue.add(node.getRight());
			}
		}
		return result;
	}
}
