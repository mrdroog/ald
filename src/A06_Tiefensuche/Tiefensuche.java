package A06_Tiefensuche;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import A05_Breitensuche.BaseTree;
import A05_Breitensuche.Node;

public class Tiefensuche extends BaseTree<Film> {

    @Override
    /**
     * Sortierkriterium im Baum: L�nge des Films
     */
    protected int compare(Film a, Film b) {
        if (a.getL�nge() < b.getL�nge())
            return -1;
        if (a.getL�nge() > b.getL�nge())
            return 1;
        return 0;
    }

    /**
     * Retourniert die Titelliste der Film-Knoten des Teilbaums in symmetrischer Folge (engl. in-order, d.h. links-Knoten-rechts)
     *
     * @param node Wurzelknoten des Teilbaums
     * @return Liste der Titel in symmetrischer Reihenfolge
     */
    public List<String> getNodesInOrder(Node<Film> node) {
        List<String> titles = new ArrayList<>();
        if (node != null) {
            titles.addAll(getNodesInOrder(node.getLeft()));
            titles.add(node.getValue().getTitel());
            titles.addAll(getNodesInOrder(node.getRight()));
        }
        return titles;
    }

    /**
     * Retourniert Titelliste jener Filme, deren L�nge zwischen min und max liegt, in Hauptreihenfolge (engl. pre-order, d.h. Knoten-links-rechts)
     *
     * @param min Minimale L�nge des Spielfilms
     * @param max Maximale L�nge des Spielfilms
     * @return Liste der Filmtitel in Hauptreihenfolge
     */
    public List<String> getMinMaxPreOrder(double min, double max) {
        List<String> titles = new ArrayList<>();
        Node<Film> node;
        Stack<Node> stack = new Stack<>();
        stack.push(root);

        while (!stack.empty()) {
            node = stack.pop();
            if (node.getValue().getL�nge() < max && node.getValue().getL�nge() > min)
                titles.add(node.getValue().getTitel());
            if (node.getRight() != null)
                stack.push(node.getRight());
            if (node.getLeft() != null)
                stack.push(node.getLeft());
        }
        return titles;
    }
}
