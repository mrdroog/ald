package A03_DoubleLinkedList;

public class DoubleLinkedList<T>
{
    private Node<T> current;
    private Node<T> last;
    private Node<T> first;

    /**
     * Einf�gen einer neuen <T>
     * @param a <T>
     */
    public void add(T a) {
        Node<T> node = new Node<>(a);
        if (first != null){
            last.setNext(node);
            node.setPrevious(last);
            node.setNext(null);
            last = node;
        } else {
            first = node;
            last = node;
        }
    }

    /**
     * Internen Zeiger f�r next() zur�cksetzen
     */
    public void reset() {
        current = first;
    }

    /**
     * analog zur Funktion reset()
     */
    public void resetToLast() {
        current = last;
    }

    /**
     * Liefert erste Node der Liste retour oder null, wenn Liste leer
     * @return Node|null
     */
    public Node<T> getFirst() {
        if (first != null)
            return first;
    	return null;
    }
    
    /**
     * Liefert letzte Node der Liste retour oder null, wenn Liste leer
     * @return Node|null
     */
    public Node<T> getLast() {
        if (last != null)
            return last;
    	return null;
    }
    
    /**
     * Gibt aktuelle <T> zur�ck und setzt internen Zeiger weiter.
     * Falls current nicht gesetzt, wird null retourniert.
     * @return <T>|null
     */
    public T next() {
        Node<T> node = current;
        if (current != null){
            current = current.getNext();
            return node.getData();
        }
    	return null;
    }

    /**
     * analog zur Funktion next()
     * @return <T>|null
     */
    public T previous() {
        Node<T> node = current;
        if (current != null){
            current = current.getPrevious();
            return node.getData();
        }
    	return null;
    }
    
    /**
     * Current-Pointer auf n�chste <T> setzen (aber nicht auslesen).
     * Ignoriert still, dass current nicht gesetzt ist.
     */
    public void moveNext() {
        if (current != null)
            current = current.getNext();
    }
    
    /**
     * Analog zur Funktion moveNext()
     */
    public void movePrevious() {
        if (current != null)
            current = current.getPrevious();
    }
   
    /**
     * Retourniert aktuelle (current) <T>, ohne Zeiger zu �ndern
     * @return <T>
     * @throws CurrentNotSetException
     */
    public T getCurrent() throws CurrentNotSetException {
        if (current != null)
            return current.getData();
        else
            throw new CurrentNotSetException();
    }

    /**
     * Gibt <T> an bestimmter Position zur�ck
     * @param pos Position, Nummerierung startet mit 1
     * @return <T>|null
     */
    public T get(int pos) {
        int counter = 1;    //Nummerierung startet mit 1.
        if (first != null){ //Wenn Liste nicht leer.
            Node<T> node = first;   //Container f�r Node die momentan betrachtet wird.
            while (counter < pos){  //Solange Betrachtungscounter kleiner als gesuchte Position.
                node = node.getNext();  //N�chste node betrachten.
                counter++; //Betrachtungscounter um 1 erh�hen.
            }
            return node.getData();  //Datenobjekt der gesuchten Node zur�ckliefern
        }
        return null; //Wenn Liste leer "null" zur�ckliefern
    }

    /**
     * Entfernen des Elements an der angegebenen Position.
     * Falls das entfernte Element das aktuelle Element ist, wird current auf null gesetzt.
     * @param pos
     */
    public void remove(int pos) {
        int counter = 1;
        if (first != null){
            Node<T> node = first;
            while (counter < pos){
                node = node.getNext();
                counter++;
            }
            if (current == node)
                current = null;
            if (node.getPrevious() != null)
                node.getPrevious().setNext(node.getNext());
            else
                first = node.getNext();
            if (node.getNext() != null)
                node.getNext().setPrevious(node.getPrevious());
            else
                last = node.getPrevious();
        }
    }
    
    /**
     * Entfernt das aktuelle Element.
     * Als neues aktuelles Element wird der Nachfolger gesetzt oder
     * (falls kein Nachfolger) das vorhergehende Element 
     * @throws CurrentNotSetException
     */
    public void removeCurrent() throws CurrentNotSetException {
        if (current != null){
            Node<T> node = current;

            if (node.getNext() != null)
                current = current.getNext();
            else if (node.getPrevious() != null)
                current = current.getPrevious();
            else
                current = null;

            if (node.getPrevious() != null)
                node.getPrevious().setNext(node.getNext());
            else
                first = node.getNext();

            if (node.getNext() != null)
                node.getNext().setPrevious(node.getPrevious());
            else
                last = node.getPrevious();
        } else
            throw new CurrentNotSetException();
    }
    
    /**
     * Die Methode f�gt die �bergebene <T> nach der aktuellen (current) ein
     * und setzt dann die neu eingef�gte <T> als aktuelle (current) <T>.
     * @throws CurrentNotSetException 
     */
    public void insertAfterCurrentAndMove(T a) throws CurrentNotSetException {
        if (current != null){
            Node<T> node = new Node<>(a);
            node.setPrevious(current);
            node.setNext(current.getNext());
            if (current.getNext() != null)
                current.getNext().setPrevious(node);
            else
                last = node;
            current.setNext(node);
            current = node;
        } else
            throw new CurrentNotSetException();
    }
}
